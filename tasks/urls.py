from django.urls import path
from tasks.views import create_task, create_list


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", create_list, name="show_my_tasks"),
]
